// stores/user.ts
import { defineStore } from 'pinia';
import type { User } from '@/types/User';

export const useUserStore = defineStore('user', () => {
  const state = {
    users: [] as User[],
    editedUser: {
      id: -1,
      email: '',
      password: '',
      fullname: '',
      gender: 'male',
      roles: ['user'],
    } as User,
    dialog: false,
    dialogDelete: false,
    loading: false,
  };

  const initialUser: User = {
    id: -1,
    email: '',
    password: '',
    fullname: '',
    gender: 'male',
    roles: ['user'],
  };

  let editedIndex = -1;

  const closeDelete = () => {
    state.dialogDelete = false;
    state.editedUser = { ...initialUser };
  };

  const deleteItemConfirm = () => {
    state.users.splice(editedIndex, 1);
    closeDelete();
  };

  const editItem = (item: User) => {
    editedIndex = state.users.indexOf(item);
    state.editedUser = { ...item };
    state.dialog = true;
  };

  const deleteItem = (item: User) => {
    editedIndex = state.users.indexOf(item);
    state.editedUser = { ...item };
    state.dialogDelete = true;
  };

  const closeDialog = () => {
    state.dialog = false;
    state.editedUser = { ...initialUser };
    editedIndex = -1;
  };

  const save = () => {
    if (editedIndex > -1) {
      // Update existing user
      state.users[editedIndex] = { ...state.editedUser };
    } else {
      // Add new user with a unique ID
      state.editedUser.id = state.users.length + 1;
      state.users.push({ ...state.editedUser });
    }
    closeDialog();
  };

  const initialize = () => {
    state.users = [
      // Your initial user data
    ];
  };

  return {
    ...state,
    closeDelete,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDialog,
    save,
    initialize,
  };
});
